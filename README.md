# Ansible in Docker

## Quick start

```
docker run --rm \
    -v $PWD:/ansible \
    -v ~/.ssh/:/root/.ssh \
    --network='host' \
    envot/ansible-dockerized
```


## Get started
Customize your playbook and hosts file.
```
docker run --rm \
    -e PLAYBOOK=playbook.yml \
    -e INVENTORY=hosts \
    -v $PWD:/ansible \
    -v ~/.ssh/:/root/.ssh \
    --network='host' \
    envot/ansible-dockerized
```


## To install roles

```
docker run --rm \
    -v $PWD:/ansible \
    -v ~/.ssh/:/root/.ssh \
    envot/ansible-dockerized \
    ansible-galaxy install -r /ansible/requirements.yml
```


## Update ansible docker

```
docker pull envot/ansible-dockerized
```


## For Maintainer

Tag and push
```
git tag -a 8.5.1-alpine -m "Ansible:8.5.1 on Alpine:3.19.0"
git push --all
```

Build test image
```
docker build -t envot/ansible-docker .
```

