FROM alpine:3.20.1

LABEL org.opencontainers.image.authors="Klemens Schueppert schueppi@envot.io"

WORKDIR /ansible/

RUN apk add --no-cache git openssh-client ansible=9.5.1-r0

RUN mkdir /etc/ansible \
    && ln -s /ansible/roles /etc/ansible/roles

ENV PLAYBOOK=playbooks/main.yml
ENV INVENTORY=inventory/

CMD ansible-playbook --ssh-common-args "-F /dev/null" -i $INVENTORY $PLAYBOOK
